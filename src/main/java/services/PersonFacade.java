/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package services;

import entities.Know;
import entities.Person;
import java.util.List;
import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.Transactional;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.WebTarget;

/**
 *
 * @author FiguerX
 */
@Path("persons")
@Stateless
@Transactional
public class PersonFacade extends AbstractFacade<Person> {

    @PersistenceContext(unitName = "personsPU")
    private EntityManager em;

    
    @Inject
    KnowFacade knowFacade;
    
    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public PersonFacade() {
        super(Person.class);
    }

    @Override
    @DELETE
    public void remove(Person entity) {
        List<Know> knows= knowFacade.findKnowsByPersonName(entity.getName());
        
        Client client = null;
        WebTarget userTarget = null;
  
        for(int i=0; i<knows.size();i++){
            client = ClientBuilder.newClient();
            userTarget = client.target("https://"+knows.get(i).getKnowPK().getServer()+"/resource/knows/{id}");
            userTarget
                .resolveTemplate("id", knows.get(i).getKnowPK().getKnownpersonname())
                .request("application/json").delete();
        }
        super.remove(entity); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    @PUT
    public void edit(Person entity) {
        super.edit(entity); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    @POST
    public void create(Person entity) {

        super.create(entity); //To change body of generated methods, choose Tools | Templates.
    }



}
