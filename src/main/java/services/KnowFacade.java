/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package services;

import entities.Know;
import entities.KnowPK;
import java.util.List;
import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Entity;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.Form;
import javax.ws.rs.core.MediaType;

/**
 *
 * @author FiguerX
 */
@Stateless
@Path("/knows")
public class KnowFacade extends AbstractFacade<Know> {

    @PersistenceContext(unitName = "personsPU")
    private EntityManager em;

    @Inject
    PersonFacade personFacade;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public KnowFacade() {
        super(Know.class);
    }

    @Override
    public int count() {
        return super.count(); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public List<Know> findAll() {

        return super.findAll(); //To change body of generated methods, choose Tools | Templates.
    }

    @GET
    @Path("{id}")
    @Produces(MediaType.APPLICATION_JSON)
    public Know find(Object id) {
        return super.find(id); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    @DELETE
    public void remove(Know entity) {
        super.remove(entity); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    @POST
    public void create(Know entity) {
        super.create(entity); //To change body of generated methods, choose Tools | Templates.
        Client client = ClientBuilder.newClient();
        WebTarget target = client.target(entity.getKnowPK().getServer()).path("resource/knows");

        Know remoteKnow = new Know();
        remoteKnow.setKnowPK(new KnowPK(entity.getPersonname().getName(), "localhost:8080"));
        remoteKnow.setPersonname(personFacade.find(entity.getKnowPK().getKnownpersonname()));

        target.request(MediaType.APPLICATION_JSON_TYPE)
                .post(Entity.entity(remoteKnow, MediaType.APPLICATION_FORM_URLENCODED_TYPE));
    }

    public List<Know> findKnowsByPersonName(String name) {
        return getEntityManager().createNamedQuery("Know.findByPersonNameName").setParameter("name", name).getResultList();
    }

}
