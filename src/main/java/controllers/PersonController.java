package controllers;

import entities.Person;
import javax.inject.Named;
import javax.faces.view.ViewScoped;

@Named(value = "personController")
@ViewScoped
public class PersonController extends AbstractController<Person> {

    public PersonController() {
        // Inform the Abstract parent controller of the concrete Person Entity
        super(Person.class);
    }

}
