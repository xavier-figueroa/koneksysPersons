package controllers;

import entities.Telephone;
import javax.inject.Named;
import javax.faces.view.ViewScoped;

@Named(value = "telephoneController")
@ViewScoped
public class TelephoneController extends AbstractController<Telephone> {

    public TelephoneController() {
        // Inform the Abstract parent controller of the concrete Telephone Entity
        super(Telephone.class);
    }

}
