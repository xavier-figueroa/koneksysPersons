package controllers;

import entities.Know;
import javax.inject.Named;
import javax.faces.view.ViewScoped;

@Named(value = "knowController")
@ViewScoped
public class KnowController extends AbstractController<Know> {

    public KnowController() {
        // Inform the Abstract parent controller of the concrete Know Entity
        super(Know.class);
    }

    @Override
    protected void initializeEmbeddableKey() {
        this.getSelected().setKnowPK(new entities.KnowPK());
    }

}
