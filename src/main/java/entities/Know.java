package entities;

import java.io.Serializable;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;

@Entity
@NamedQueries({
    @NamedQuery(name = "Know.findAll", query = "SELECT k FROM Know k"),
    @NamedQuery(name = "Know.findByPersonNameName", query = "SELECT k FROM Know k where k.personname.name = :name")
    , @NamedQuery(name = "Know.findByKnownpersonname", query = "SELECT k FROM Know k WHERE k.knowPK.knownpersonname = :knownpersonname")
    , @NamedQuery(name = "Know.findByServer", query = "SELECT k FROM Know k WHERE k.knowPK.server = :server")})
public class Know implements Serializable {

    private static final long serialVersionUID = 1L;
    @EmbeddedId
    protected KnowPK knowPK;
    @JoinColumn(name = "PERSONNAME", referencedColumnName = "NAME")
    @ManyToOne(optional = false)
    private Person personname;

    public Know() {
    }

    public Know(KnowPK knowPK) {
        this.knowPK = knowPK;
    }

    public Know(String knownpersonname, String server) {
        this.knowPK = new KnowPK(knownpersonname, server);
    }

    public KnowPK getKnowPK() {
        return knowPK;
    }

    public void setKnowPK(KnowPK knowPK) {
        this.knowPK = knowPK;
    }

    public Person getPersonname() {
        return personname;
    }

    public void setPersonname(Person personname) {
        this.personname = personname;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (knowPK != null ? knowPK.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Know)) {
            return false;
        }
        Know other = (Know) object;
        if ((this.knowPK == null && other.knowPK != null) || (this.knowPK != null && !this.knowPK.equals(other.knowPK))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "entities.Know[ knowPK=" + knowPK + " ]";
    }
    
}
