package entities;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

@Entity
@NamedQueries({
    @NamedQuery(name = "Telephone.findAll", query = "SELECT t FROM Telephone t")
    , @NamedQuery(name = "Telephone.findByTelephonenumber", query = "SELECT t FROM Telephone t WHERE t.telephonenumber = :telephonenumber")})
public class Telephone implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 256)
    private String telephonenumber;
    @JoinColumn(name = "PERSONNAME", referencedColumnName = "NAME")
    @ManyToOne(optional = false)
    private Person personname;

    public Telephone() {
    }

    public Telephone(String telephonenumber) {
        this.telephonenumber = telephonenumber;
    }

    public String getTelephonenumber() {
        return telephonenumber;
    }

    public void setTelephonenumber(String telephonenumber) {
        this.telephonenumber = telephonenumber;
    }

    public Person getPersonname() {
        return personname;
    }

    public void setPersonname(Person personname) {
        this.personname = personname;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (telephonenumber != null ? telephonenumber.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Telephone)) {
            return false;
        }
        Telephone other = (Telephone) object;
        if ((this.telephonenumber == null && other.telephonenumber != null) || (this.telephonenumber != null && !this.telephonenumber.equals(other.telephonenumber))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "entities.Telephone[ telephonenumber=" + telephonenumber + " ]";
    }
    
}
