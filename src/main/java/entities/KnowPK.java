/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entities;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Embeddable;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 *
 * @author FiguerX
 */
@Embeddable
public class KnowPK implements Serializable {

    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 256)
    private String knownpersonname;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 256)
    private String server;

    public KnowPK() {
    }

    public KnowPK(String knownpersonname, String server) {
        this.knownpersonname = knownpersonname;
        this.server = server;
    }

    public String getKnownpersonname() {
        return knownpersonname;
    }

    public void setKnownpersonname(String knownpersonname) {
        this.knownpersonname = knownpersonname;
    }

    public String getServer() {
        return server;
    }

    public void setServer(String server) {
        this.server = server;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (knownpersonname != null ? knownpersonname.hashCode() : 0);
        hash += (server != null ? server.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof KnowPK)) {
            return false;
        }
        KnowPK other = (KnowPK) object;
        if ((this.knownpersonname == null && other.knownpersonname != null) || (this.knownpersonname != null && !this.knownpersonname.equals(other.knownpersonname))) {
            return false;
        }
        if ((this.server == null && other.server != null) || (this.server != null && !this.server.equals(other.server))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "entities.KnowPK[ knownpersonname=" + knownpersonname + ", server=" + server + " ]";
    }
    
}
