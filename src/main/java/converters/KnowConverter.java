package converters;

import entities.Know;
import services.KnowFacade;
import controllers.util.JsfUtil;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.faces.convert.FacesConverter;
import javax.inject.Inject;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;

@FacesConverter(value = "knowConverter")
public class KnowConverter implements Converter {

    @Inject
    private KnowFacade ejbFacade;

    private static final String SEPARATOR = "#";
    private static final String SEPARATOR_ESCAPED = "\\#";

    @Override
    public Object getAsObject(FacesContext facesContext, UIComponent component, String value) {
        if (value == null || value.length() == 0 || JsfUtil.isDummySelectItem(component, value)) {
            return null;
        }
        return this.ejbFacade.find(getKey(value));
    }

    entities.KnowPK getKey(String value) {
        entities.KnowPK key;
        String values[] = value.split(SEPARATOR_ESCAPED);
        key = new entities.KnowPK();
        key.setKnownpersonname(values[0]);
        key.setServer(values[1]);
        return key;
    }

    String getStringKey(entities.KnowPK value) {
        StringBuffer sb = new StringBuffer();
        sb.append(value.getKnownpersonname());
        sb.append(SEPARATOR);
        sb.append(value.getServer());
        return sb.toString();
    }

    @Override
    public String getAsString(FacesContext facesContext, UIComponent component, Object object) {
        if (object == null
                || (object instanceof String && ((String) object).length() == 0)) {
            return null;
        }
        if (object instanceof Know) {
            Know o = (Know) object;
            return getStringKey(o.getKnowPK());
        } else {
            Logger.getLogger(this.getClass().getName()).log(Level.SEVERE, "object {0} is of type {1}; expected type: {2}", new Object[]{object, object.getClass().getName(), Know.class.getName()});
            return null;
        }
    }

}
